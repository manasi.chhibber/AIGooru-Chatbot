from langchain.llms import OpenAI 
from langchain.document_loaders import DirectoryLoader
from langchain.vectorstores import FAISS 
from langchain.embeddings import OpenAIEmbeddings 
from langchain.chains import ConversationalRetrievalChain 
from langchain.memory import ConversationBufferMemory 
from langchain.text_splitter import RecursiveCharacterTextSplitter 
import streamlit as st

import streamlit as st
import openai

st.set_page_config(
    page_title='AIGooru Chatbot',
    layout="wide",
    initial_sidebar_state="collapsed",
    menu_items={'About': f"## AIGooru Chatbot\n**Developed by** : Cognozire"}
)

st.markdown("""<style> footer {visibility: hidden;} </style>""", unsafe_allow_html=True)
page_bg_img = f"""<style>[data-testid="stAppViewContainer"] > .main {{background-image: url("https://img.freepik.com/free-vector/hand-drawn-colorful-science-education-wallpaper_23-2148489183.jpg?w=2000");background-size: cover;background-position: top left;background-repeat: no-repeat;}}[data-testid="stHeader"] {{background: rgba(0,0,0,0);}}</style>"""
st.markdown(page_bg_img, unsafe_allow_html=True)

c1, c2, c3 = st.columns([0.1, 0.04, 0.25])
c3.markdown("<h1 style=font-size: 48px;'>AIGooru Chatbot</h1>", unsafe_allow_html=True)

st.markdown('<style>div.block-container{padding-top:1rem;}</style>', unsafe_allow_html=True)

openai.api_key = st.secrets['openai']['OPENAI_API_KEY']
model = 'gpt-3.5-turbo'
c1, c2 = st.sidebar.columns(2)
if c1.button('Retrain Model'):
    with st.spinner('Retraining Model... It may take several minutes... Please Wait...'):
        pass

def chatgpt_qa(query, model):
    conversation = [
        {"role": "system", "content": "Assistant is a large language model trained by OpenAI."},
        {"role": "user", "content": query}
    ]
    response = openai.ChatCompletion.create(
        model=model,
        messages=conversation,
        temperature=0.2
    )
    return response.choices[0].message.content.strip()

def model_qa(query):
    pdf_loader = DirectoryLoader('Files/', glob="**/*.pdf")
    txt_loader = DirectoryLoader('Files/', glob="**/*.txt")
    html_loader = DirectoryLoader('Files/', glob="**/*.html")
    word_loader = DirectoryLoader('Files/', glob="**/*.docx")
    excel_loader = DirectoryLoader('Files/', glob="**/*.xlsx")
    loaders = [pdf_loader, txt_loader, html_loader, word_loader, excel_loader]
    documents = []
    for loader in loaders:
        documents.extend(loader.load())

    documents = RecursiveCharacterTextSplitter( 
    chunk_size=1000, 
    chunk_overlap=100, 
    length_function=len).split_documents(documents) 

    faiss_index = FAISS.from_documents(documents=documents, embedding=OpenAIEmbeddings(openai_api_key=st.secrets['openai']['OPENAI_API_KEY']))

    retriever = faiss_index.as_retriever() 

    memory = ConversationBufferMemory( 
    memory_key='chat_history', 
    return_messages=True, 
    output_key='answer')

    chain = ConversationalRetrievalChain.from_llm( 
    llm=OpenAI(openai_api_key=st.secrets['openai']['OPENAI_API_KEY']), 
    retriever=retriever, 
    memory=memory 
    ) 

    return chain({'question': query})['answer'].strip()

query = st.text_input("**Enter your Query**")
search_mode = st.radio('Select Database', options=['AIGooru', 'Internet', 'Both'], horizontal=True)

if st.button("**Search**"):
    if query == '':
        st.warning("Enter your query before pressing search")
    else:
        label = {'AIGooru': 'AIGooru custom', 'Internet': 'ChatGPT', 'Both': 'AIGooru custom database and ChatGPT'}
        with st.spinner(f"Getting Answer from {label[search_mode]}... Wait for it..."):
            if search_mode == 'AIGooru':
                # Search in AIGooru database
                qa = model_qa(query)
                st.text_area(f"**Answer from {label[search_mode]} database**", qa, height=300)
            elif search_mode == 'Internet':
                # Search using ChatGPT API
                text = chatgpt_qa(query, model)
                st.text_area(f"**Answer from {label[search_mode]} database**", text, height=300)
            else:
                c1, c2 = st.columns(2)
                # Search in AIGooru database
                qa = model_qa(query)
                c1.text_area(f"**Answer from AIGooru custom database**", qa, height=300)
                # Search using ChatGPT API
                text2 = chatgpt_qa(query, model)
                c2.text_area(f"**Answer from ChatGPT database**", text2, height=300)
