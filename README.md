# AIGooru-Chatbot

### Try the app here: https://aigooru-chatbot.streamlit.app/

### For re-training:

1. Drop pdf/txt/html/docx/xlsx files in the `Files` folder.
2. Refresh the app.
